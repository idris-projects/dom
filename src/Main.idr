module Main 

import TParsec
import TParsec.Running
import Parser.Html

main : IO ()
main = do 
  htmlFile <- readFile "index.html"
  case htmlFile of
       Left err => putStrLn "Error"
       Right htmlFile => do 
          putStrLn htmlFile
          let htmlTest = parseMaybe (pack $ filter (/= '\n') $ unpack htmlFile) parseHTML
          case htmlTest of
            Nothing => putStrLn "Nothing"
            (Just html) =>
                case html of
                    x => putStrLn $ show x

