module Parser.Html

import TParsec
import TParsec.Running

import Data.SizedDict
import Data.NEList

%default total
%access public export
   
data HtmlToken = LABRACKET | LENDABRACKET | RABRACKET | SPACE | TAGHTML | TAGDIV | TAGHEAD | TAGBODY

data HTML : Type where
  Html : List HTML -> HTML
  Head  : List HTML -> HTML
  Body  : List HTML -> HTML
  Div  : List HTML -> HTML

Show HTML where 
  show (Html  []) = "Html []" 
  show (Html  [Html []]) = "Html [Html []]" 
  show (Html [Div []]) = "Html [Div []]"
  show (Div []) = "Div []"
  show (Div [Div []]) = "Div [Div []]"
  show (Div [Html []]) = "Div [Html []]"
  show (Html [Head [], Body [Div []]])  = "Html [Head [], Body [Div []]]"
 -- show (Div html) = "Div "  -- ++ " " ++ show html
  show _ = ""

appendHtml : HTML -> HTML -> HTML 
appendHtml (Html []) (Div []) = Html [Div []]
appendHtml x _ = x

Eq HtmlToken where
  (==) LABRACKET    LABRACKET     = True
  (==) LENDABRACKET LENDABRACKET  = True
  (==) RABRACKET    RABRACKET     = True
  (==) TAGHTML      TAGHTML       = True
  (==) TAGDIV       TAGDIV        = True
  (==) TAGHEAD      TAGHEAD       = True
  (==) TAGBODY      TAGBODY       = True
  (==) SPACE        SPACE         = True
  (==) _            _             = False
  
Tokenizer HtmlToken where
  tokenize = toHtmlToken . unpack
    where
      toHtmlToken : List Char -> List HtmlToken 
      toHtmlToken [] = []
      toHtmlToken ('<'::'/'::xs) = LENDABRACKET :: toHtmlToken xs
      toHtmlToken ('<'::xs) = LABRACKET :: toHtmlToken xs
      toHtmlToken ('>'::xs) = RABRACKET :: toHtmlToken xs
      toHtmlToken ('-'::xs) = SPACE :: toHtmlToken xs 
      toHtmlToken ('h'::'t'::'m'::'l'::xs) = TAGHTML :: toHtmlToken xs 
      toHtmlToken ('h'::'e'::'a'::'d'::xs) = TAGHEAD :: toHtmlToken xs 
      toHtmlToken ('b'::'o'::'d'::'y'::xs) = TAGBODY :: toHtmlToken xs 
      toHtmlToken ('d'::'i'::'v'::xs) = TAGDIV :: toHtmlToken xs 
      toHtmlToken (x::xs) = toHtmlToken xs 

HTMLParser : Type -> Nat -> Type
HTMLParser = Parser TParsecU (sizedtok HtmlToken)

exactHtmlToken : HtmlToken -> All (HTMLParser HtmlToken)   
exactHtmlToken = exact

anyHtml : All (HTMLParser HtmlToken)   
anyHtml = anyTok
 
begBrakets : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
begBrakets = between (exactHtmlToken LABRACKET)  (exactHtmlToken RABRACKET)

endBrakets : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
endBrakets = between (exactHtmlToken LENDABRACKET) (exactHtmlToken RABRACKET)

html : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
html = between  (begBrakets $ exactHtmlToken TAGHTML) (endBrakets $ exactHtmlToken TAGHTML)

html' : All (Box (HTMLParser (HtmlToken,HtmlToken)) :-> (HTMLParser (HtmlToken,HtmlToken)))
html' = between  (begBrakets $ exactHtmlToken TAGHTML) (endBrakets $ exactHtmlToken TAGHTML)

head : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
head = between  (begBrakets $ exactHtmlToken TAGHEAD) (endBrakets $ exactHtmlToken TAGHEAD)

body : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
body = between  (begBrakets $ exactHtmlToken TAGBODY) (endBrakets $ exactHtmlToken TAGBODY)

div : All (Box (HTMLParser HtmlToken) :-> (HTMLParser HtmlToken))
div = between  (begBrakets $ exactHtmlToken TAGDIV) (endBrakets $ exactHtmlToken TAGDIV)

parseHTML : All (HTMLParser HTML)
parseHTML =
  {- let -}
    {- t = begBrakets $ exactHtmlToken TAGHTML  -}
    {- t2 = begBrakets $ exactHtmlToken TAGDIV  -}
    {- expr   = hchainl (cmap (Html []) t) ((cmap appendHtml t)) (cmap (Div []) $ html t2)  -}
  {- in expr -}
  
  alts [
     cmap (Html [Head [], Body [Div []]]) $ html' $ (head anyHtml) `and`  (body $ div anyHtml),
     cmap (Html [Div []]) $ html $ div anyHtml,
     cmap (Html []) $ html anyHtml
  ]

